import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (

    <div className="container-fluid">
      <h1>Health At Home!</h1>
      <h2>Nav</h2>
      <p>Basic horizontal menu:</p>
      <ul className="nav">
        <li className="nav-item">
          <a className="nav-link" href="#">Model 1</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Model 2</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Model 3</a>
        </li>
        <li className="nav-item">
          <a className="nav-link disabled" href="#">About</a>
        </li>
      </ul>
    </div>


  );
}

export default App;
